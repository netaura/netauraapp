'use strict';

import React, { Component } from 'react';
import { useIsFocused, useFocusEffect } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {
  ViroARSceneNavigator,
  ViroARScene,
  Viro3DObject,
  ViroLightingEnvironment,
  ViroARImageMarker,
  ViroARTrackingTargets,
  ViroSpotLight,
  ViroQuad
} from '@viro-community/react-viro';

import {
  Dimensions,
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

// At the top where our imports are...
import VideoPlayer from 'react-native-video-controls';
import DescriptionComponent from '../components/DescriptionComponent';

// https://github.com/computationalcore/react-native-trivia-quiz/blob/master/src/Scaling.js
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
//const guidelineBaseWidth = 350;
//const guidelineBaseHeight = 680;
const guidelineBaseWidth = 500;
const guidelineBaseHeight = 972;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor;

import tips from '../utils/tips'

function ARScene({ onUpdate }) {
  useFocusEffect(
    React.useCallback(() => {
      onUpdate("focus");

      return () => onUpdate("unfocus");
    }, [onUpdate])
  );

  return null;
}

class TrackerScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAR: false,
      show: false,
      video: undefined,
      triggers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
      oCatch: []
    }

    // bind 'this' to functions
    this._onAnchorFound = this._onAnchorFound.bind(this);
    this._mapReturn = this._mapReturn.bind(this);
    this._getImageTracker = this._getImageTracker.bind(this);
    this._handleUpdate = this._handleUpdate.bind(this);
    this._getTip = this._getTip.bind(this);
    this._getColor = this._getColor.bind(this);
    this._getCatcheds = this._getCatcheds.bind(this);
    this._putCatcheds = this._putCatcheds.bind(this);

    this._getCatcheds()
  }

  _onAnchorFound(id) {
    this.state.oCatch.push(id);
    this.setState({ show: true, tip: tips[id] });

    this._putCatcheds()
  }

  async _getCatcheds(){
    const jsonValue = await AsyncStorage.getItem('@oCatch');
    this.state.oCatch = jsonValue != null ? JSON.parse(jsonValue) : [];
  }

  async _putCatcheds(){
    const jsonValue = JSON.stringify( this.state.oCatch );    
    await AsyncStorage.setItem('@oCatch', jsonValue);
  }

  _mapReturn(callback) {
    return callback.map(value => {
      return (
        <ViroARImageMarker 
          target={`trigger${value + 1}`} onAnchorFound={() => this._onAnchorFound(value)} >
        </ViroARImageMarker>
      );
    })
  }

  _getImageTracker() {
    return (
      <ViroARScene>
        {this._mapReturn(this.state.triggers)}
      </ViroARScene>
    )
  }

  _handleUpdate(str) {
    if (str == "focus") {
      // this.setState({ show: true  })
      setTimeout(
        () => {
          this.setState({ showAR: true }) /* navigation.navigate('Escanear Carta'); */
        },
        1000
      );
    }

    if (str == "unfocus") this.setState({ showAR: false })
  }

  _getTip(id){
    if ( this.state.oCatch.indexOf(id) != -1 ){
      this.setState({ show: true, tip: tips[id] });
    }
  }

  _getColor(id) {
    if ( this.state.tip == tips[id] ) return '#57e35c';
    if ( this.state.oCatch.indexOf(id) != -1 ) return '#7ed6ff';
    
    return 'white';
  }

  render() {
    return (
      <View style={styles.outer} >
        <ARScene onUpdate={this._handleUpdate} />

        {this.state.showAR
          && <ViroARSceneNavigator initialScene={{ scene: this._getImageTracker }} />}

        {true && this.state.show &&
          <View style={{alignItems: 'center'}}>
            <View style={styles.videoContainer}>
              <View style={styles.tip}>
                <View style={styles.title}>
                  <Text style={styles.titleText}>
                    {this.state.tip.title}
                    {"\n"}
                  </Text>
                  <TouchableHighlight onPress={ () => { this.setState({ show: false, tip: undefined }) } } style={{ width:"20%" }}>
                    <Text style={styles.close}>
                      { "X" }
                    </Text>
                  </TouchableHighlight>
                </View>
                <VideoPlayer style={{height: '80%'}} source={this.state.tip.video}/>
                <DescriptionComponent style={{height: '10%'}} tipText={this.state.tip.descrition}/>
              </View>
            </View>
          </View>
        }

        <View style={{alignItems: 'center'}}>
          <View style={styles.videoContainer}>
            <View style={styles.tipDots}>
              {
                this.state.triggers.map((userData) =>
                  <TouchableHighlight 
                    style={{ 
                      height: height * 0.6 / 16, 
                      backgroundColor: this._getColor(userData),
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: 0.4,
                      borderColor: '#eee',
                    }} 
                    onPress={ () => { this._getTip(userData) } }>
                    <Text style={{textAlign: 'center', justifyContent:'center', textWeigth:'bold', color:"#555" }}>
                      { userData + 1 }
                    </Text>
                  </TouchableHighlight>
                )
              }
            </View>
          </View>
        </View>

      </View>
    );
  }
}

ViroARTrackingTargets.createTargets({
  // Precisar ser quadradas a imagens?
  trigger1: {
    source: require('../res/trigger/1.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger2: {
    source: require('../res/trigger/2.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger3: {
    source: require('../res/trigger/3.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger4: {
    source: require('../res/trigger/4.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger5: {
    source: require('../res/trigger/5.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger6: {
    source: require('../res/trigger/6.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger7: {
    source: require('../res/trigger/7.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger8: {
    source: require('../res/trigger/8.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger9: {
    source: require('../res/trigger/9.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger10: {
    source: require('../res/trigger/10.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger11: {
    source: require('../res/trigger/11.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger12: {
    source: require('../res/trigger/12.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger13: {
    source: require('../res/trigger/13.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger14: {
    source: require('../res/trigger/14.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger15: {
    source: require('../res/trigger/15.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  },
  trigger16: {
    source: require('../res/trigger/16.jpg'),
    orientation: "Up",
    physicalWidth: 0.165 // real world width in meters
  }
});

var styles = StyleSheet.create({
  outer: {
    height: '100%'
  },
  viroContainer: {
    height: '20%',
  },
  buttonText: {
    position: 'absolute',
    top: -500,
    color: '#fff',
    textAlign: 'center',
    fontSize: 20
  },
  questionDataContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center'
  },
  videoContainer: {
    position: 'relative'
  },
  title: {
    height: '10%',
    backgroundColor: '#630d93',
    flexDirection: 'row', 
    justifyContent: 'flex-end'
  },
  titleText: {
    width: '60%',
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: '#630d93',
    padding: 10,
    textAlign: 'center',
    color: '#eee'
  },
  close: {
    zIndex: 1,
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: '#560482',
    padding: 10,
    textAlign: 'center',
    color: '#eee'
  },
  tip: {
    position: 'absolute',
    top: height * -0.8,
    backgroundColor: 'rgb(52, 52, 52)',
    width: width * 0.8,
    height: height * 0.6,
    left: width * -0.4,
    flex:1, 
    flexDirection:'column', 
    justifyContent:'center'
  },
  tipDots: {
    position: 'absolute',
    top: height * -0.8,
    backgroundColor: '#eee',
    width: width * 0.1,
    height: height * 0.6,
    left: width * -0.5,
    flex: 1, 
    flexDirection:'column', 
    justifyContent:'center'
  },
  video: {
    height: '80%'
  },
  buttons: {
    position: "relative",
    height: 80,
    width: 200,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  }
});

export default TrackerScreen;
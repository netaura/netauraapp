import React, { Component } from 'react';
import { StyleSheet, Text, SafeAreaView, ScrollView, StatusBar, View,TouchableHighlight } from 'react-native';

class DescriptionComponent extends Component {
  constructor(props){
    super(props);
    this.state = {
      scrollViewHeight: 0,
      currentXOffset: 0
    }

    this.leftArrow = this.leftArrow.bind(this);
    this.rightArrow = this.rightArrow.bind(this);
  }

  leftArrow = () => {
    let eachItemOffset = this.state.scrollViewHeight / 10; 
    let _currentXOffset =  this.state.currentXOffset - eachItemOffset;
    
    if ( _currentXOffset < 0 ){
      _currentXOffset = 0;
    }
    
    this.scrollview.scrollTo({x: 0, y: _currentXOffset, animated: true});
    this.setState({ currentXOffset: _currentXOffset })
  }

  rightArrow = () => {
    let eachItemOffset = this.state.scrollViewHeight / 10; 
    let _currentXOffset = this.state.currentXOffset + eachItemOffset;
    
    if ( _currentXOffset >= this.state.scrollViewHeight ){
      _currentXOffset = this.state.scrollViewHeight - eachItemOffset;
    }
    
    this.scrollview.scrollTo({x: 0, y: _currentXOffset, animated: true});
    this.setState({ currentXOffset: _currentXOffset })
  }

  render() {
    return (
      // style={{flex:1, flexDirection:'row', justifyContent:'center'}}
      <View style={styles.scrollView}>
        { false && <View style={styles.buttonContainer}>
          <TouchableHighlight 
            style={styles.button}
            onPress={this.leftArrow}>
            <Text>  </Text>
          </TouchableHighlight>
        </View> }
        <ScrollView //style={styles.scrollView}>
          showsVerticalScrollIndicator={true} 
          showsHorizontalScrollIndicator={false}
          // scrollEnabled={false}
          ref={ ref => {
            this.scrollview = ref;
          }}
          onContentSizeChange={(w, h) => this.setState({ scrollViewHeight: h })}
          style={{ backgroundColor: '#98dfff' }}
          >
            <Text style={styles.text}>
              { this.props.tipText }
            </Text>
        </ScrollView>
        { false && <View style={styles.buttonContainer}>
          <TouchableHighlight 
            style={styles.button}
            onPress={this.rightArrow}>
            <Text> { ">" } </Text>
          </TouchableHighlight>
        </View> }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#f6fcff',
    flexDirection:'row',
    maxHeight: '30%',
    alignItems: 'center'
  },
  text: {
    backgroundColor: '#f6fcff',
    padding: 10,
    fontSize: 18
  },
  somestyle: {
    paddingVertical:10,
    paddingHorizontal:20,
    margin:10,
    borderWidth:1,
    borderRadius:1,
    borderColor:'black'
  },
  button: {
    textAlign: 'center', 
    padding: 10,
    margin: 5,
    backgroundColor:'grey', 
    borderRadius: 50,
    borderWidth: 2,
    width: 50, 
    height: 50
  }
});

export default DescriptionComponent;
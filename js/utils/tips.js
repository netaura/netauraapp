let tip1  = require('../res/tips/1.mp4');
let tip2  = require('../res/tips/2.mp4');
let tip3  = require('../res/tips/3.mp4');
let tip4  = require('../res/tips/4.mp4');
let tip5  = require('../res/tips/5.mp4');
let tip6  = require('../res/tips/6.mp4');
let tip7  = require('../res/tips/7.mp4');
let tip8  = require('../res/tips/8.mp4');
let tip9  = require('../res/tips/9.mp4');
let tip10 = require('../res/tips/10.mp4');
let tip11 = require('../res/tips/11.mp4');
let tip12 = require('../res/tips/12.mp4');
let tip13 = require('../res/tips/13.mp4');
let tip14 = require('../res/tips/14.mp4');
let tip15 = require('../res/tips/15.mp4');
let tip16 = require('../res/tips/16.mp4');

const tips = [
  { 
    title: "Dica 1",
    video: tip1,
    descrition : 
    "O primeiro computador da fila está conectado sem fio pelo padrão IEEE 802.11n."
  },
  { 
    title: "Dica 2",
    video: tip2,
    descrition : 
    "O computador ligado pelo enlace com maior atraso de propagação tem habilitado como outro serviço um Relay Router do TOR."
  },
  { 
    title: "Dica 3",
    video: tip3,
    descrition : 
    "O último computador da fila, cuja implementação do TCP possui recuperação rápida, tem como outro serviço um Tracker de uma rede P2P."
  },
  { 
    title: "Dica 4",
    video: tip4,
    descrition : 
    "O computador conectado por Li-Fi tem um servidor de aplicação que utiliza um protocolo de transporte não confiável."
  },
  { 
    title: "Dica 5",
    video: tip5,
    descrition : 
    "O computador ligado por fibra-ótica tem um servidor de aplicação Web."
  },
  { 
    title: "Dica 6",
    video: tip6,
    descrition : 
    "O último computador da fila tem um endereço IPv6."
  },
  { 
    title: "Dica 7",
    video: tip7,
    descrition : 
    "O computador com IP de número IP_1 possui um Servidor de Aplicação de streaming de áudio."
  },
  { 
    title: "Dica 8",
    video: tip8,
    descrition : 
    "O computador com IP_1 está localizado do lado esquerdo do computador cujo endereço IPv6 é de uma rede local."
  },
  { 
    title: "Dica 9",
    video: tip9,
    descrition : 
    "O computador com IP_5  suporta o protocolo de transporte TCP (Vegas)."
  },
  { 
    title: "Dica 10",
    video: tip10,
    descrition : 
    "O computador localizado exatamente no meio da fila tem um servidor de um Jogo Distribuído."
  },
  { 
    title: "Dica 11",
    video: tip11,
    descrition : 
    "O computador cuja aplicação principal utiliza um protocolo da camada de transporte rUDP tem como outro serviço instalado o SSH."
  },
  { 
    title: "Dica 12",
    video: tip12,
    descrition : 
    "O computador com NAT opera na primeira posição da fila de computadores."
  },
  { 
    title: "Dica 13",
    video: tip13,
    descrition : 
    "O computador com Wi-Fi fica ao lado esquerdo do computador com endereço IP_3."
  },
  { 
    title: "Dica 14",
    video: tip14,
    descrition : 
    "O computador que utiliza a camada de transporte TCP (Tahoe) fica ao lado direito do computador com um servidor de nomes com autoridade."
  },
  { 
    title: "Dica 15",
    video: tip15,
    descrition : 
    "O computador com um servidor SMTP está ao lado direito do computador com Wi-Fi."
  },
  { 
    title: "Dica 16",
    video: tip16,
    descrition : 
    "O computador ligado por cabo par trançado possui endereço IPv4 para redes locais."
  }
]

export default tips;
# NetauraAPP

Repositório destinado ao aplicativo mobile do jogo AmongNet.

![imagem de exemplo](https://gitlab.com/netaura/netauraapp/-/raw/master/sample.png)

## Instalação
```
npm install
```
## Executar Metro
```
npx react-native start
```
## Execução
```
npx react-native run-android
```

Construído com ViroReact (https://github.com/ViroCommunity/starter-kit.git).
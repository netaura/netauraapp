/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

import TrackerScreen from './js/screens/TrackerScreen';

export default class NetauraApp extends Component {
  constructor() {
    super();

    this.state = {

    }
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
              headerShown: false
            }}
          />
          <Stack.Screen name="Profile" options={{ title: 'Scanner' }} component={TrackerScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const HomeScreen = ({ navigation }) => {
  return (
    <View style={localStyles.outer} >
      <View style={localStyles.inner} >

        <Text style={localStyles.titleText}>
          NetAura
        </Text>

        <TouchableHighlight style={localStyles.buttons}
          onPress={() => navigation.navigate('Profile', { name: 'Jane' })}
          underlayColor={'#68a0ff'} >

          <Text style={localStyles.buttonText}>Start</Text>
        </TouchableHighlight>
      </View>
    </View>
  );
};

var localStyles = StyleSheet.create({
  viroContainer: {
    flex: 1,
    backgroundColor: "black",
  },
  outer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: "#fff",
  },
  inner: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: "#fff",
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 20,
    color: '#8313c1',
    textAlign: 'center',
    fontSize: 38,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20
  },
  buttons: {
    height: 80,
    width: 200,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#02a8f4',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  exitButton: {
    height: 50,
    width: 100,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  }
});

module.exports = NetauraApp

/*


    "react-native-gesture-handler": "^1.10.3",
    "react-native-maps": "0.26.1",
    "react-native-reanimated": "^1.4.0",
    "react-native-screens": "^1.0.0-alpha.23",
    "react-navigation": "^4.0.10",
    "react-navigation-stack": "^1.10.3",

*/